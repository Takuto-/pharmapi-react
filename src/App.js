import React, { Component } from 'react';
import './App.css';
import './index.css';
import Navbar from '../src/components/NavBar.js';
import Card from '../src/components/Cards.js';
import Footer from '../src/components/Footer.js';
import Admin from './components/Admin.js';
import AddForm from './components/AddForm.js'


export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      page: ''
    }

    this.showPage = this.showPage.bind(this);
  }

  showPage(str) {
    this.setState({
      page: str
    });
  }

  render() {

    if (this.state.page === "accueil") {
      return (
        <div className='app'>
          <Navbar showPage={(page) => this.showPage(page)} />
          <Card />
          <Footer />
        </div>


      );
    } else if (this.state.page === "garde") {
      return (
        <div className='app'>
          <Navbar showPage={(page) => this.showPage(page)} />
          <Card />
          <Footer />
        </div>
      )
    } else if (this.state.page === "admin") {
      return (
       <div className="adminPageStyle">
       <Navbar showPage={(page) => this.showPage(page)} />
       <Admin showPage={(page) => this.showPage(page)}/>
       
       <Footer />
       </div>
      )
    } else if (this.state.page === "add") {
      return (
        <div className="formPageStyle">
          <Navbar showPage={(page) => this.showPage(page)} />
          <AddForm />
        </div>
      )
    }
    return (
      <div className='app'>
        <Navbar showPage={(page) => this.showPage(page)} />
        <Card />
        <Footer />
      </div>
    )
  }

}

