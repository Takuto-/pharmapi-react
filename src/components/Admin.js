import React, { Component } from 'react'
import axios from 'axios'
import AddForm from './AddForm';

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      showForm: false,
      nom: '',
      ville: '',
      quartier: '',
      garde: ''

    };

    this.handleChange = this.handleChange.bind(this);
    this.addPharm = this.addPharm.bind(this);
  }

  componentDidMount() {
    axios.get(`http://localhost:8000/pharma`)
      .then(res => {
        const data = res.data;
        this.setState({ data });
      })
  }

  deletePharm(id) {
    axios.delete(`http://127.0.0.1:8000/pharma/${id}`)
      .then(res => {

        const data = this.state.data.filter(item => item.id !== id);
        this.setState({ data });
      })
  }

  addPharm(e) {
    e.preventDefault();
    const pharm = {
      nom: this.state.nom,
      quartier: this.state.quartier,
      ville: this.state.ville,
      garde: this.state.garde
    }
       axios.post(`http://127.0.0.1:8000/pharma`,pharm )
       .then(res => {
         console.log(res)
         })
       }

         
     

  showForm(str) {
    this.setState({
      showForm: str
    });
  }

  handleChange(e) {
    this.setState({ [e.target.id]: e.target.value });
    console.log(this.state.quartier)
  };








  render() {

    const { data } = this.state;
    return (

      <div>
        {this.state.showForm === true ?

          <form onSubmit={this.addPharm}>
            <label>
              Nom :
          <input type="text" id="nom" value={this.state.nom} onChange={this.handleChange} />

            </label>
            <label>
              Quartier :
          <input type="text" id="quartier" value={this.state.quartier} onChange={this.handleChange}></input></label>
            <label>
              Ville :
          <input type="text" id="ville" value={this.state.ville} onChange={this.handleChange}></input></label>
            <label>
              Cette pharmacie est de garde?:
          <input type="text" id="garde" value={this.state.garde} onChange={this.handleChange}></input></label>
            <input type="submit" value="Envoyer"></input>
          </form> : ""}


        <button onClick={() => this.showForm(true)} type="button" class="btn btn-success">Ajouter une Pharmacie</button>
        {data.map((data, index) => (<div className='cardStyle'>
          <div key={index} className="card border-primary mb-3">
            <div className="card-header">{data.nom}</div>
            <div className="card-body text-primary">
              <h6 className="card-title">Quartier: {data.quartier}</h6>
              <p className="card-text">Ville: {data.ville}</p>
              <p className="card-text">Cette pharmacie est de garde le: {data.garde}</p>
              <div className="d-grid gap-2 d-md-block">
                <button onClick={() => this.props.showPage('modifier')} className="btn btn-primary" type="button">modifier</button>
                <button onClick={() => this.deletePharm(data.id)} className="btn btn-primary ms-2" type="button">supprimer</button>
              </div>
            </div>
          </div>
        </div>)
        )}
      </div>
    )
  }
}


export default Admin

