import React, { Component, Fragment } from 'react'
import axios from 'axios'



class Card extends Component {
    constructor(props) {
        super(props);
    }
  
    state = {
        data: []

    }



    componentDidMount() {
        axios.get(`http://localhost:8000/pharma`)
            .then(res => {
                const data = res.data;
                this.setState({ data });
            })
    }

    render() {

        return this.state.data.length && this.state.data.map((data, index) => {
            return (

                <Fragment key={index}>
                    <div className='cardStyle'>
                        <div className="card border-primary mb-3">
                            <div className="card-header">{data.nom}</div>
                            <div className="card-body text-primary">
                                <h6 className="card-title">Quartier: {data.quartier}</h6>
                                <p className="card-text">Ville: {data.ville}</p>
                                <p className="card-text">Cette pharmacie est de garde le: {data.garde}</p>
                            </div>
                        </div>
                    </div>

                </Fragment>

            )
        })

    }
}

export default Card