import React, { Component } from 'react'
import Logo from '../ph.png'


class Navbar extends Component {
    constructor(props) {
        super(props);
    }

    showPage(page) {
        this.props.showPage(page);
    }


    render() {
       
        return (

            <nav className="navbar navbar-expand-lg navbar-light">
                <div className="container-fluid">
                    <img src={Logo} onClick={() => this.props.showPage('accueil')} alt='logo' className='logoStyle'></img>

                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarText">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                            </li>
                            <li className="nav-item">
                                <a onClick={() => this.props.showPage('garde')} className="nav-link">Pharmacie de garde</a>

                            </li>
                            <li className="nav-item">
                                <a onClick={() => this.props.showPage('admin')} className="nav-link">Administer une Pharmacie</a>
                            </li>
                        </ul>
                        <span className="navbar-text">
                            Pharmapi c'est pour la vie
                </span>
                    </div>
                </div>
            </nav>



        )
    }
}

export default Navbar